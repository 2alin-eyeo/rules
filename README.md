# @adblockinc/rules

@adblockinc/rules is an NPM package for retrieving, storing and providing filter
lists from various sources for use in AdBlock and Adblock Plus.

## Requirements

- Node >= 16.10.0; < 17.0.0
- npm >= 7.0.0; < 8.0.0
- @eyeo/webext-sdk 0.6.0

## Setup

Run `npm install`.

## Usage

### Update rules

1. Run `npm run update {adblock|adblockplus}`.
2. Commit updated content of "data" directory.
3. Update dependency in the extension repository.

### Build distribution files

Run `npm run build {adblock|adblockplus}`.

### Integration into extension builds

In the extension repository:

1. Run `rules {adblock|adblockplus}`.
2. Build extension using generated files:
    - Include content of `dist/manifest/*.json` in manifest.json file.
    - Include `dist/rules/abp/*` in build as `data/rules/abp/*`.
    - Include `dist/rules/dnr/*` in build as `data/rules/dnr/*`.
    - Pass content of `dist/index/*.json` to `EWE.start()`.  
      _Note: The index file can also be accessed via @adblockinc/rules/* (e.g.
      `import index from "@adblockinc/rules/adblockplus`)._

## Content

|Location|File|Content|Curation|
|-|-|-|-|
|build|config/*.js|Product-specific list of recommended filter lists|manual|
|data / dist[^data]|index/eyeo.json|Index of available filter lists provided by eyeo|[automatic](#usage)|
|data / dist[^data]|index/*.json|Index of product-specific filter lists|[automatic](#usage)|
|data / dist[^data]|manifest/*.json|Product-specific manifest file fragments|[automatic](#usage)|
|data / dist[^data]|rules/abp/*|Filter list files (Adblock Plus syntax)|[automatic](#usage)|
|data / dist[^data]|rules/dnr/*|Filter list files (declarativeNetRequest syntax|[automatic](#usage)|

[^data]: Files that are stored in repository are located in "data" directory,
whereas temporary files are located in "dist" directory.

### Filter list data generation

```mermaid
graph TD;
  subgraph optional
    server-eyeo(eyeo Server) -->|EWE: subs-init| index-eyeo
  end
  index-eyeo(index/eyeo.json) --> index-selected
  config(config/*.js) --> index-selected
  server-subscriptions(Filter list servers) -->|EWE:subs-fetch| rules-abp
  index-selected(index/*.json) -->|EWE: subs-fetch| rules-abp
  rules-abp(rules/abp/*) -->|EWE: subs-convert| rules-dnr
  rules-dnr(rules/dnr/*) -->|EWE: subs-generate| manifest(manifest/*.json)
  rules-abp --> ext-rules-abp(data/rules/abp/*)
  rules-dnr --> ext-rules-dnr(data/rules/dnr/*)
  manifest --> ext-manifest(manifest.json)
  index-selected --> ext-rec("@adblockinc/rules/*")
  subgraph build
    ext-manifest --> ext(Extension build)
    ext-rec --> ext
    ext-rules-abp --> ext
    ext-rules-dnr --> ext
  end
  ext -->|EWE: start| ext-sdk(vendor/webext-sdk)

  classDef data stroke-width:2px
  classDef ext fill:#F002
  classDef dist stroke:none

  class config data
  class ext data
  class ext-manifest dist
  class ext-rec dist
  class ext-rules dist
  class index-adblockinc data
  class index-eyeo data
  class index-selected dist
  class manifest dist
  class rules-abp data
  class rules-dnr dist
```
